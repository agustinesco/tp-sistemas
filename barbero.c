#include <pthread.h>
#include <stdio.h>
#include <semaphore.h>
#include <stdlib.h>
#include <time.h>

int cant_clientes=0;
char estado_barbero='d';
int n=7;

pthread_mutex_t llave;
sem_t lugar;
sem_t gente;


void* clientes_function()
{
int i;
 while(1) {
  if(cant_clientes < n){
      //hay lugar, puedo entrar
        sem_wait(&lugar);
	pthread_mutex_lock(&llave);
        cant_clientes++;
	sem_post(&gente);
      if(estado_barbero=='c'){
         //esperar en la silla
          printf("leyendo revista...");
      }else if (estado_barbero=='d'){
         //despertar al barber
         printf("ejem, ejem.., cof, cof...");
      }
	pthread_mutex_unlock(&llave);
  }else{
      //no hay lugar, me voy
      printf("chau...");
  }
 }
}

void* barbero_function()
{
int i;
while(1){
 	if(cant_clientes==0){
    		  //dormir
		pthread_mutex_lock(&llave);
	         estado_barbero='d';
                 printf("ZZZZZ");
		pthread_mutex_unlock(&llave);
     	}else{
        	  sem_wait(&gente);
		  pthread_mutex_lock(&llave);
      	   	  cant_clientes--;
      		  //cortar pelo
      		  estado_barbero='c';
      		  printf("XXXXX");
      		  sem_post(&lugar);
		 pthread_mutex_unlock(&llave);
   		}
	}

}

int main(){
	pthread_t barbero, clientes;
	sem_init(&lugar,0 , 7);
	sem_init(&gente, 0, 0);

	pthread_create(&barbero, NULL, *barbero_function, NULL);
	pthread_create(&clientes, NULL, *clientes_function, NULL);

	pthread_join(barbero, NULL);
	pthread_join(clientes, NULL);

	sem_destroy(&lugar);
	sem_destroy(&gente);

	pthread_exit(NULL);

	return 0;
}

