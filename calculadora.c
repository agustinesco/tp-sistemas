#include <stdio.h>

void calculadora(float numero1,float numero2,char operator);
float potencia(float base, int exponente);
int main()
{

int ejecucion=1;
   while(ejecucion==1) {
	float num1;
	float num2;
	float respuesta;
	char operador;
	char continuar;

	printf("ingrese el primer numero: ");
	scanf("%f", &num1);

	printf("ingrese el operador: + , - , / , * , ^ ");

	scanf(" %c", &operador);

	printf("ingrese el segundo numero: ");
	scanf("%f", &num2);

	calculadora(num1,num2,operador);

	printf("¿desea realizar otra operacion? S/N" );
	scanf(" %c", &continuar);
	switch(continuar){
	case 'n':
		ejecucion=0;
		break;
	case 's':
		break;
	default:
		printf("opcion incorrecta");
	}
    }
	return 0;
}


void calculadora(float numero1,float numero2, char operator)
{
	float resultado;
	if( operator == '+' ){
		resultado=numero1+numero2;
	}
	else if ( operator == '-' ){
		resultado = numero1 - numero2 ;
	}
	else if ( operator == '*' ){
		resultado = numero1 * numero2;
	}
	else if ( operator == '/' ){
		resultado = numero1 / numero2;
	}
	else if (operator == '^' ){
		resultado= potencia(numero1 , (int) numero2);
	}
	else{
		printf("operador no valido");
	}
	printf("el resultado es: %f \n",resultado);
}


float potencia(float base, int exponente){
		float res= 1;
		int i;
		for (i = 0; i < exponente; i++){
			res *= base;
		}
		return res;
}
