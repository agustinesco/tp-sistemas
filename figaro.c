#include <pthread.h>
#include <stdio.h>
#include <semaphore.h>
#include <stdlib.h>

sem_t aux;
sem_t A;
sem_t B;
sem_t C;
sem_t D;
sem_t E;
sem_t F;
sem_t Fa;
sem_t Fb;
pthread_mutex_t bloqueo;



void* fiigaro_f(){
	sem_wait(&A);
   		printf("Fiiiigaro");
		printf("\n");
	sem_post(&B);
	sem_post(&C);
	sem_post(&D);
}




void* figaroa_f(){
	sem_wait(&B);
   printf("Figaro ");
	sem_post(&E);
}


void* figarob_f(){
	sem_wait(&C);
   printf(" Figaro ");
	sem_post(&Fb);

}


void* figaroc_f(){

	sem_wait(&D);
  		 printf(" Figaro");
	sem_post(&Fa);
}



void* figaro_fi_f(){
	sem_wait(&E);
	sem_wait(&Fa);
	sem_wait(&Fb);
   printf("\n");
   printf("Figaro Fi\n");
	sem_post(&F);
}


void* figaro_fa_f(){
	sem_wait(&F);
 	  printf("Figaro Fa\n");
	sem_post(&A);
}



int main(){
	pthread_t fiigaro,figaroa,figarob,figaroc,figarofi,figarofa;
	sem_init(&A,0,1);
	sem_init(&B,0,0);
	sem_init(&C,0,0);
	sem_init(&D,0,0);
	sem_init(&E,0,0);
	sem_init(&F,0,0);
	sem_init(&Fa,0,0);
	sem_init(&Fb,0,0);
	sem_init(&aux,0,0);

	pthread_create(&fiigaro, NULL , *fiigaro_f , NULL);
	pthread_create(&figaroa, NULL , *figaroa_f , NULL);
	pthread_create(&figarob, NULL , *figarob_f , NULL);
	pthread_create(&figaroc, NULL , *figaroc_f , NULL);
	pthread_create(&figarofi, NULL , *figaro_fi_f , NULL);
	pthread_create(&figarofa, NULL , *figaro_fa_f , NULL);

	pthread_join(fiigaro,NULL);
	pthread_join(figaroa,NULL);
	pthread_join(figarob,NULL);
	pthread_join(figaroc,NULL);
	pthread_join(figarofi,NULL);
	pthread_join(figarofa,NULL);

	sem_destroy(&A);
	sem_destroy(&B);
	sem_destroy(&C);
	sem_destroy(&D);
	sem_destroy(&E);
	sem_destroy(&F);
	sem_destroy(&aux);

	pthread_exit(NULL);

	return(0);

}
